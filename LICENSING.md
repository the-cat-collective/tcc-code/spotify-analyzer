## Project License
This project is licensed under the BSD 3-Clause License. Please refer to the [LICENSE](LICENSE) file for the full text of the project's license.

## Third-Party Components
1. mypy
   - Authors: mypy Development Team
   - Project GitHub: [python/mypy](https://github.com/python/mypy)
   - License: MIT License
   - Original Project's License: [LICENSE](https://github.com/python/mypy/blob/master/LICENSE)
   - Credits: [CREDITS](https://github.com/python/mypy/blob/master/CREDITS)

2. PrettyTable
   - Author: Jazzband
   - Project GitHub: [jazzband/prettytable](https://github.com/jazzband/prettytable/)
   - License: BSD License
   - Original Project's License: [LICENSE](https://github.com/jazzband/prettytable/blob/main/LICENSE)

3. PyYAML
   - Authors: YAML Team.
   - Project GitHub: [yaml/pyyaml](https://github.com/yaml/pyyaml)
   - License: MIT License
   - Original Project's License: [LICENSE](https://github.com/yaml/pyyaml/blob/master/LICENSE)

4. Spotipy
   - Author: Paul Lamere
   - Project GitHub: [spotipy-dev/spotipy](https://github.com/spotipy-dev/spotipy)
   - License: MIT License
   - Original Project's License: [LICENSE.md](https://github.com/spotipy-dev/spotipy/blob/master/LICENSE.md)

5. tkinter
   - Author: The Python Software Foundation
   - Project GitHub: [python/cpython](https://github.com/python/cpython)
   - License: Python Software Foundation License
   - Original Project's License: [LICENSE](https://github.com/python/cpython/blob/main/LICENSE)
