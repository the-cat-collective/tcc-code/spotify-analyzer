#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Imports -----------------------------------------------------------------

import os
import sys
from typing import List

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

sys.path.insert(0, os.path.abspath('../../src/spanalyzer/application'))

project = 'SPAnalyzer'
copyright = '2024, The Cat Collective'
author = 'The Cat Collective'
release = '1.0.0'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions: List[str] = [
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx_autodoc_typehints',
    'sphinx_rtd_theme',
]

autodoc_mock_imports: List[str] = [
    'spanalyzer.application.core.output_utils',
    'spanalyzer.application.core.sp_api_access',
    'spanalyzer.application.gui.gui',
    'spanalyzer.application.main.main',
    'spanalyzer.application.parsers.argument_parser',
    'spanalyzer.application.parsers.yaml_parser',
    'spanalyzer.application.utilities.exceptions',
    'spanalyzer.application.utilities.message_utils',
]

templates_path: List[str] = ['_templates']
exclude_patterns : List[str]= []

# The master toctree document.
master_doc: str = 'index'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme: str = 'sphinx_rtd_theme'
html_static_path: List[str] = ['_static']
html_css_files: List[str] = ['custom.css']

# -- Options for autodoc extension -------------------------------------------
# Automatically extract type hints when specified and place them in
# descriptions of the relevant function/method.
autodoc_typehints: str = 'description'

# Don't show class signature with the class' name.
autodoc_class_signature: str = 'separated'

# Specify the order of autodoc generation
autodoc_member_order = 'bysource'

# -- Options for napoleon extension -----------------------------------------
# Use Google-style docstrings
napoleon_google_docstring: bool = True
napoleon_numpy_docstring: bool = False
