application
===========

.. toctree::
   :maxdepth: 4

   core
   gui
   main
   parsers
   utilities
