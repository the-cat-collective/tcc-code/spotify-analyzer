#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __main__.py for SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

from spanalyzer.application.main.main import main

if __name__ == '__main__':
    main()
