#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py file for core
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.
"""
Core functionality for the program.

This module provides essential features and functionality that form the foundation of the program.
It includes classes related to output formatting and accessing Spotify's API.
"""
