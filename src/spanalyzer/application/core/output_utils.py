#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Functions relating to output from the SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

from prettytable import PrettyTable
import os
import sys

from spanalyzer.application.utilities.exceptions import UnsupportedOperationError
from spanalyzer.application.utilities.message_utils import return_location_message
from spanalyzer.application.core.sp_api_access import PlaylistData


def ensure_directory_exists(file_path: str) -> None:
    """
    Ensure that the directory of the given file path exists. If not, create it.

    Args:
        file_path (str): The file path.

    Returns:
        None
    """
    directory = os.path.dirname(file_path)
    if not os.path.exists(directory):
        os.makedirs(directory)


def get_albums_data(data: PlaylistData, sort: bool) -> str:
    """
    Get formatted album names and their corresponding index numbers.

    Args:
        data (PlaylistData): The PlaylistData object containing album names.
        sort (bool): Whether to sort the albums.

    Returns:
        str: Formatted string with album names and index numbers.
    """
    albums = sorted(set(data.album_names)) if sort else set(data.album_names)
    output = '\n'.join([f"{index+1}: {name}" for index, name in enumerate(albums)])
    return output


def store_albums_to_file(data: PlaylistData, sort: bool, filename: str) -> None:
    """
    Stores the album names, along with their index numbers, to a file.

    Args:
        data (PlaylistData): The PlaylistData object containing album names.
        sort (bool): Whether to sort the albums.
        filename (str): Full path of the output file.

    Returns:
        None
    """
    albums = sorted(set(data.album_names)) if sort else set(data.album_names)

    full_path = os.path.expanduser(filename)

    ensure_directory_exists(full_path)

    with open(full_path, 'w', encoding='utf8') as f:
        for index, name in enumerate(albums):
            f.write(f"{index+1}: {name}\n")

    print(return_location_message())


def get_songs_data(data: PlaylistData, sort: bool) -> str:
    """
    Get formatted song names and their corresponding index numbers.

    Args:
        data (PlaylistData): The PlaylistData object containing song names.
        sort (bool): Whether to sort the songs.

    Returns:
        str: Formatted string with song names and index numbers.
    """
    songs = sorted(data.track_names) if sort else data.track_names
    output = '\n'.join([f"{index+1}: {name}" for index, name in enumerate(songs)])
    return output


def store_songs_to_file(data: PlaylistData, sort: bool, filename: str) -> None:
    """
    Stores the song names, along with their order numbers, to a file.

    Args:
        data (PlaylistData): The PlaylistData object containing song names.
        sort (bool): Whether to sort the songs.
        filename (str): Full path of the output file.

    Returns:
        None
    """
    songs = sorted(data.track_names) if sort else data.track_names

    full_path = os.path.expanduser(filename)

    ensure_directory_exists(full_path)

    with open(full_path, 'w', encoding='utf8') as f:
        for index, name in enumerate(songs):
            f.write(f"{index+1}: {name}\n")

    print(return_location_message())


def get_table_data(data: PlaylistData) -> str:
    """
    Get a well-formatted table of track data.

    Args:
        data (PlaylistData): A PlaylistData object containing album names, song names, and artist names.

    Returns:
        str: Formatted string with a table of track data.
    """
    table = PrettyTable()
    table.field_names = ['Index', 'Album', 'Song', 'Artist']
    table.align['Album'] = 'c'
    table.align['Song'] = 'c'
    table.align['Artist'] = 'c'

    table.padding_width = 1

    for index, (album, song, artist) in enumerate(
        zip(data.album_names, data.track_names, data.artist_names)
    ):
        table.add_row([index + 1, album, song, artist])

    return str(table)


def store_table_to_file(data: PlaylistData, filename: str) -> None:
    """
    Stores a well-formatted table of track data to a file.

    Args:
        data (PlaylistData): A PlaylistData object containing album names, song names, and artist names.
        filename (str): Path of the output file relative to the user's home directory.

    Returns:
        None
    """
    table = PrettyTable()
    table.field_names = ['Index', 'Album', 'Song', 'Artist']
    table.align['Album'] = 'c'
    table.align['Song'] = 'c'
    table.align['Artist'] = 'c'

    table.padding_width = 1

    for index, (album, song, artist) in enumerate(
        zip(data.album_names, data.track_names, data.artist_names)
    ):
        table.add_row([index + 1, album, song, artist])

    full_path = os.path.expanduser(filename)

    ensure_directory_exists(full_path)

    with open(full_path, 'w', encoding='utf8') as f:
        f.write(str(table))

    print(return_location_message())


def process_and_output(
    data: PlaylistData,
    output_method: str,
    sort: bool,
    data_output_type: str,
    filename: str,
    suppress_output: bool = False,
) -> str:
    """
    Process and output track data using the specified method.

    This function processes a set of track data and returns a formatted string or PrettyTable object,
    depending on the selected output method. The track data is processed using specific functions
    for album, song, or table output.

    Args:
        data (PlaylistData): The PlaylistData object containing track data.
        output_method (str): The output method, either "screen" or "file."
        sort (bool): Whether to sort the processed data.
        data_output_type (str): Type of data to process (e.g., "album", "song", "table").
        filename (str): Full path of the output file.
        suppress_output (bool): Whether to suppress printing to the console.

    Returns:
        str: Formatted string or PrettyTable object.
    """
    gui_mode = 'gui' in sys.argv

    result = ''

    if gui_mode:
        # GUI mode: Only printing to GUI is supported
        if output_method.lower() == 'screen':
            if data_output_type.lower() == 'album':
                result = get_albums_data(data, sort)
            elif data_output_type.lower() == 'song':
                result = get_songs_data(data, sort)
            elif data_output_type.lower() == 'table':
                result = get_table_data(data)
            else:
                raise UnsupportedOperationError('Invalid data output type')

            if not suppress_output:
                print(result)
        elif output_method.lower() == 'file':
            raise UnsupportedOperationError('File writing is not supported in GUI mode')
        else:
            raise UnsupportedOperationError('Invalid output option')
    else:
        # Non-GUI mode: Follow previous behavior
        if data_output_type.lower() == 'album':
            if output_method.lower() == 'screen':
                result = get_albums_data(data, sort)
            elif output_method.lower() == 'file':
                store_albums_to_file(data, sort, filename)
            else:
                raise UnsupportedOperationError('Invalid output option')
        elif data_output_type.lower() == 'song':
            if output_method.lower() == 'screen':
                result = get_songs_data(data, sort)
            elif output_method.lower() == 'file':
                store_songs_to_file(data, sort, filename)
            else:
                raise UnsupportedOperationError('Invalid output option')
        elif data_output_type.lower() == 'table':
            if output_method.lower() == 'screen':
                result = get_table_data(data)
            elif output_method.lower() == 'file':
                store_table_to_file(data, filename)
            else:
                raise UnsupportedOperationError('Invalid output option')

        if not suppress_output:
            print(result)

    return result
