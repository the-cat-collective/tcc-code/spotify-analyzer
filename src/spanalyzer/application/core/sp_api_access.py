#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Functions relating to interacting with Spotify's API utilized by SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

from typing import List
from dataclasses import dataclass

from spotipy import Spotify
from spotipy.oauth2 import SpotifyClientCredentials


@dataclass
class PlaylistData:
    album_names: List[str]
    track_names: List[str]
    artist_names: List[str]


def send_api_requests(
    client_id: str, client_secret: str, playlist_url: str
) -> PlaylistData:
    """
    Send API requests to Spotify to retrieve information about tracks in a playlist.

    Args:
        client_id (str): The Spotify client ID.
        client_secret (str): The Spotify client secret.
        playlist_url (str): The URL of the Spotify playlist.

    Returns:
        PlaylistData: An object containing data about the playlist, including
                      album names, track names, and artist names.

    Raises:
        spotipy.SpotifyException: If there is an issue with the Spotify API request.

    Note:
        This function retrieves information about tracks in a Spotify playlist,
        including album names, track names, and artist names. It uses the Spotipy
        library to interact with the Spotify Web API.
    """
    client_credentials_manager = SpotifyClientCredentials(
        client_id=client_id, client_secret=client_secret
    )
    sp = Spotify(client_credentials_manager=client_credentials_manager)

    offset = 0
    album_names: List[str] = []
    track_names: List[str] = []
    artist_names: List[str] = []

    while True:
        results = sp.playlist_items(playlist_url, offset=offset, limit=100)
        tracks = results['items']

        if not tracks:
            break

        album_names.extend(track['track']['album']['name'] for track in tracks)
        track_names.extend(track['track']['name'] for track in tracks)
        artist_names.extend(
            artist['name'] for track in tracks for artist in track['track']['artists']
        )

        offset += len(tracks)

    return PlaylistData(album_names, track_names, artist_names)
