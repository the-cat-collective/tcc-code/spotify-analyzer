#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py file for gui
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.
"""
Handles graphical user interface (GUI) components.

This submodule manages all aspects related to the program's graphical user interface.
It includes classes and functions for creating windows and tables.
"""
