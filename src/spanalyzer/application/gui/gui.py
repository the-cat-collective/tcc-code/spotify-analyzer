#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Code for SpAnalyzer's GUI
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

import tkinter as tk
from tkinter import scrolledtext, ttk

from spanalyzer.application.utilities.exceptions import UnsupportedOperationError
from spanalyzer.application.core.output_utils import process_and_output
from spanalyzer.application.utilities.message_utils import return_welcome_message
from spanalyzer.application.core.sp_api_access import PlaylistData


class SpotifyGUI:
    """Class representing the Spotify API GUI."""

    def __init__(
        self,
        master: tk.Tk,
        api_data: PlaylistData,
        sort: bool,
        output_type: str,
        filename: str,
    ) -> None:
        """
        Initialize the SpotifyGUI.

        Args:
            master (tk.Tk): The master Tkinter window.
            api_data (PlaylistData): The PlaylistData object containing track data.
            sort (bool): Whether to sort the processed data.
            output_type (str): Type of data to process (e.g., "album", "song", "table").
            filename (str): Full path of the output file (placeholder).
        """
        self.master = master
        master.title('Spotify API GUI')

        # Display welcome message as a pop-up
        self.show_welcome_message()

        master.geometry('1280x720')

        # Title bar
        title_label = tk.Label(master, text='Spotify API GUI', font=('Helvetica', 16))
        title_label.pack(pady=10)

        # Create scrolled text widget for output
        self.output_text = scrolledtext.ScrolledText(
            master, wrap=tk.WORD, width=80, height=20
        )
        self.output_text.pack(expand=True, fill='both', padx=10, pady=10)

        # Create a vertical scroll bar for the text widget
        scroll_bar = ttk.Scrollbar(master, command=self.output_text.yview)
        scroll_bar.pack(side=tk.RIGHT, fill=tk.Y)
        self.output_text.config(yscrollcommand=scroll_bar.set)

        def update_gui() -> None:
            try:
                result = process_and_output(
                    api_data,
                    output_method='screen',
                    sort=sort,
                    data_output_type=output_type,
                    filename=filename,
                    suppress_output=True,
                )

                # Update the text widget with the result
                self.output_text.insert(tk.END, result)

            except UnsupportedOperationError as e:
                self.output_text.insert(tk.END, f"Error: {e}")

            master.after(100, update_gui)

        update_gui()

        def on_closing() -> None:
            """Callback for when the GUI is closed."""
            master.destroy()

        master.protocol('WM_DELETE_WINDOW', on_closing)

        # Bind arrow keys for scrolling
        self.output_text.bind('<Up>', lambda _: self.scroll_up())
        self.output_text.bind('<Down>', lambda _: self.scroll_down())

    def show_welcome_message(self) -> None:
        """Display the welcome message as a pop-up."""
        welcome_message = return_welcome_message()

        top = tk.Toplevel(self.master)
        top.title('Disclaimer')

        top.geometry('500x300')

        welcome_label = tk.Message(top, text=welcome_message, width=400)
        welcome_label.pack(padx=10, pady=10)

        top.attributes('-topmost', True)

        top.lift()
        top.wait_window()

    def scroll_up(self) -> None:
        """Scroll the text widget up."""
        self.output_text.yview_scroll(-1, 'units')

    def scroll_down(self) -> None:
        """Scroll the text widget down."""
        self.output_text.yview_scroll(1, 'units')
