#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py file for main
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.
"""
Acts as the main entry point for the program.

This submodule serves as the primary entry point for the program's execution.
It includes the main script or module responsible for initializing and starting the program.
It is normally called by a dummy __main__.py entry point in the parent directory.
"""
