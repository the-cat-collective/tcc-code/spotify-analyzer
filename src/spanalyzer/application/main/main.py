#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Code for SpAnalyzer's Main
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

"""Usage: $ python spotify-analyzer.py [--config PATHNAME] --client_id CLIENT_ID --client_secret CLIENT_SECRET
--playlist_url PLAYLIST_URL --output {screen,file} [--sort] [--filename FILENAME] [--gui]

Arguments:
  --config,    -Y       YAML Configuration file path (conditionally required).
  --client_id, -C       Spotify Client ID (required).
  --client_secret, -S   Spotify Client Secret (required).
  --playlist_url, -U    Spotify Playlist URL (required).
  --output, -o          Choose the output method ('screen' or 'file') (required).
  --sort, -s            Sort the output set (required).
  --filename, -F        Output filename when using file output (optional).

Options:
  --gui, -G             Run the Spotify Analyzer in GUI mode (optional).

Examples:
  1. Analyze a Spotify playlist and display the results on the screen:
     $ python spotify-analyzer.py -C YOUR_CLIENT_ID -S YOUR_CLIENT_SECRET -U PLAYLIST_URL -o screen

  2. Analyze a Spotify playlist, save the results to a file, and sort them:
     $ python spotify-analyzer.py -C YOUR_CLIENT_ID -S YOUR_CLIENT_SECRET -U PLAYLIST_URL -o file -s

  3. Run the Spotify Analyzer in GUI mode:
     $ python spotify-analyzer.py -C YOUR_CLIENT_ID -S YOUR_CLIENT_SECRET -U PLAYLIST_URL --gui
"""

import argparse
import tkinter as tk

from spanalyzer.application.gui.gui import SpotifyGUI
from spanalyzer.application.parsers.argument_parser import parse_arguments
from spanalyzer.application.utilities.message_utils import (
    return_welcome_message,
    return_completion_message,
)
from spanalyzer.application.core.sp_api_access import send_api_requests, PlaylistData
from spanalyzer.application.core.output_utils import process_and_output


def main() -> None:
    """Main function to run the Spotify API application."""
    args: argparse.Namespace = parse_arguments()
    print(return_welcome_message())
    api_return: PlaylistData = send_api_requests(
        args.client_id, args.client_secret, args.playlist_url
    )

    if args.gui:
        # Create an instance of SpotifyGUI and run the Tkinter main loop
        root = tk.Tk()
        gui = SpotifyGUI(root, api_return, args.sort, args.type, args.file)
        root.mainloop()
    else:
        process_and_output(
            api_return, args.output.lower(), args.sort, args.type, args.file
        )

    print(return_completion_message())
