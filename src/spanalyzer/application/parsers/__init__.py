#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py file for parsers
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.
"""
Provides parsers for data processing.

This submodule is dedicated to parsing and processing data within the program.
It contains modules, classes, and functions responsible for handling input data via cmdline args and YAML.
"""
