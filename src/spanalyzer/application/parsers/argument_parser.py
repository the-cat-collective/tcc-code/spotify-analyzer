#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Command Line Parser abstraction for SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

import argparse

from spanalyzer.application.utilities.exceptions import ConfigError
from spanalyzer.application.parsers.yaml_parser import load_config


def parse_arguments() -> argparse.Namespace:
    """
    Parse command-line arguments for the Spotify Playlist Analyzer.
    """
    parser = argparse.ArgumentParser(description='Spotify Analyzer')

    parser.add_argument(
        '-Y',
        '--config',
        nargs='?',
        const='config.yaml',
        help='Path to YAML configuration file',
    )
    parser.add_argument('--gui', '-G', action='store_true', help='Use the GUI mode')
    parser.add_argument('--client_id', '-C', help='Spotify Client ID')
    parser.add_argument('--client_secret', '-S', help='Spotify Client Secret')
    parser.add_argument(
        '--file', '-f', nargs='?', const='output.txt', help='Store to a file'
    )
    parser.add_argument(
        '--output',
        '-o',
        choices=['screen', 'file'],
        help='Pick either file storage or printing to the screen',
    )
    parser.add_argument('--playlist_url', '-U', help='Spotify Playlist URL')
    parser.add_argument('--sort', '-s', action='store_true', help='Sort the output set')
    parser.add_argument(
        '--type', '-T', help='Specify the type of output: song, album, or table format'
    )

    args = parser.parse_args()

    if args.config == 'config.yaml':
        print(f"Using default configuration file: {args.config}")

    # If args.config is the set mode, do not allow for any other args to be set
    if args.config:
        config = load_config(args.config)
        if config is None:
            raise ConfigError(
                'Error: Required keys not found in the configuration file.'
            )

        if any(
            [
                args.client_id,
                args.client_secret,
                args.gui,
                args.output,
                args.playlist_url,
                args.sort,
                args.type,
            ]
        ):
            parser.error(
                'Error: Command-line arguments are not allowed when providing a configuration file (-Y).'
            )

        args.client_id = config.get('client_id', None)
        args.client_secret = config.get('client_secret', None)
        args.file = config.get('file', None)
        args.gui = config.get('gui', None)
        args.output = config.get('output', None)
        args.playlist_url = config.get('playlist_url', None)
        args.sort = config.get('sort', None)
        args.type = config.get('type', None)

    # Make some arguments required conditionally
    required_args = [
        'client_id',
        'client_secret',
        'file',
        'gui',
        'output',
        'playlist_url',
        'sort',
        'type',
    ]
    for arg in required_args:
        if getattr(args, arg) is None:
            parser.error(f"Error: {arg} is required.")

    return args
