#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Functions to parse the YAML configuration file for SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.

import sys
import os
from typing import Dict, Optional, Union
import yaml

from spanalyzer.application.utilities.exceptions import ConfigError


def load_config(file_path: str) -> Optional[Dict[str, Union[str, bool]]]:
    """
    Load configuration from a YAML file.

    Args:
        file_path (str): Path to the YAML configuration file.

    Returns:
        Optional[Dict[str, Union[str, bool]]]: A dictionary containing the configuration,
        or None if the file does not contain the required keys.
    """
    try:
        with open(file_path, 'r') as file:
            config = yaml.safe_load(file)

            # If the config file includes Spotify credentials, use them
            if 'Spotify' in config:
                spotify_config = config['Spotify']
                required_keys = [
                    'client_id',
                    'client_secret',
                    'file',
                    'output',
                    'playlist_url',
                    'sort',
                    'type',
                ]

                for key in required_keys:
                    if key not in spotify_config or not spotify_config[key]:
                        raise ConfigError(
                            f"Error: Required key '{key}' not found or empty in the configuration file."
                        )

                sort = spotify_config.get('sort')
                if sort is None:
                    sort = False
                else:
                    if str(sort).lower() == 'true':
                        sort = True
                    elif str(sort).lower() == 'false':
                        sort = False
                    else:
                        raise ConfigError('Invalid value set for sort')

                gui = spotify_config.get('gui')
                if gui is None:
                    gui = False
                else:
                    if str(gui).lower() == 'true':
                        gui = True
                    elif str(gui).lower() == 'false':
                        gui = False
                    else:
                        raise ConfigError('Invalid value set for gui')

                return {
                    'client_id': spotify_config['client_id'],
                    'client_secret': spotify_config['client_secret'],
                    'file': os.path.join(
                        os.path.expanduser('~'), 'results', spotify_config['file']
                    ),
                    'gui': gui,
                    'output': spotify_config['output'],
                    'playlist_url': spotify_config['playlist_url'],
                    'sort': sort,
                    'type': spotify_config['type'],
                }

    except FileNotFoundError:
        print(f"Warning: Config file '{file_path}' not found.")
        create_config = input(
            'Do you want to create the config file? (yes/no): '
        ).lower()
        if create_config != 'yes':
            print('Aborting.')
            sys.exit()

        create_config_file(file_path)
        print(
            f"Edit the config file '{file_path}' and replace the placeholders with your Spotify credentials and associated information. When you are done, re-run the script"
        )
        sys.exit()
    except yaml.YAMLError as e:
        raise ConfigError(f"Error: Unable to parse the configuration file. {e}")

    return None


def create_config_file(file_path: str) -> None:
    """
    Create a new YAML configuration file with placeholders.

    Args:
        file_path (str): Path to the YAML configuration file.
    """
    default_file_path = 'output.txt'

    config_data = {
        'Spotify': {
            'client_id': None,
            'client_secret': None,
            'file': default_file_path,
            'gui': None,
            'output': None,
            'playlist_url': None,
            'sort': None,
            'type': None,
        }
    }

    with open(file_path, 'w') as file:
        yaml.dump(config_data, file, default_flow_style=False)
