#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# __init__.py file for utilities
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.
"""
Offers utility functions for common tasks.

The 'utilities' submodule contains a collection of utility functions and types that provide common functionality across the program.
The 'utilities' submodule contains all custom exception types used in the program along with functions to print the formatted entry and quit messages.
"""
