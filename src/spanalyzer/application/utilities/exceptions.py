#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# File containing custom exception classes utilized internally within SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.


class ConfigError(Exception):
    """Exception raised for configuration-related errors."""

    pass


class UnsupportedOperationError(Exception):
    """Exception raised when caller tries to run an unsupported series of operations"""

    pass
