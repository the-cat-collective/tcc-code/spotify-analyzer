#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Functions to print headers and footers within SpAnalyzer
#
# SPDX-FileCopyrightText: 2024 The Cat Collective <calculuscat@proton.me>
# SPDX-License-Identifier: BSD-3-Clause
#
# Note: The programs and configurations used by this script may not be under the same license.
# Please check the LICENSING file in the root directory of this repository for more information.
#
# This script was created by The Cat Collective.


def return_welcome_message() -> str:
    """
    Return a welcome message as a string.

    Displays a welcome message when the SpAnalyzer is executed.

    Returns:
        str: Welcome message.

    Example:
        To retrieve and display the welcome message:

        >>> welcome_message = return_welcome_message()
        >>> print(welcome_message)

    Note:
        This function is designed to be used as part of SpAnalyzer.
        It is not intended to be run independently outside of the program.
    """

    return """
        ===========================================================================
        This software is unofficial and not related to or endorsed by Spotify AB or
        any of its subsidiaries. This software is created by a college student on a
        free day. Please don't expect too much and just enjoy the show!
        ===========================================================================
        """


def return_completion_message() -> str:
    """
    Return a completion message as a string.

    Displays a completion message when the SpAnalyzer has finished its execution.

    Returns:
        str: Completion message.

    Example:
        To retrieve and display the completion message:

        >>> completion_message = return_completion_message()
        >>> print(completion_message)

    Note:
        This function is designed to be used as part of SpAnalyzer.
        It is not intended to be run independently outside of the program.
    """

    return """
        ===========================================================================
        Done! Enjoy!
        ===========================================================================
        """


def return_location_message() -> str:
    """
    Return a message indicating the location of the output file as a string.

    Displays a message indicating the location of the output file when the SpAnalyzer has finished its execution.

    Returns:
        str: Location message.

    Example:
        To retrieve and display the location message:

        >>> location_message = return_location_message()
        >>> print(location_message)

    Note:
        This function is designed to be used as part of SpAnalyzer.
        It is not intended to be run independently outside of the program.
    """

    return """
        ===========================================================================
        Your file is located within ~/results. Enjoy!
        ===========================================================================
        """
